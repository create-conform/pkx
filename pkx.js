/////////////////////////////////////////////////////////////////////////////////////////////
//
// pkx
//
//    Using extension module that enables the loading of pkx packages.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var host =             require("host");
var event =            require("event");
var io =               require("io");
var ioURI =            require("io-uri");
var ioVolume =         require("io-volume");
var tar =              require("io-format-tar");
var gzip =             require("io-format-gzip");
var ioBufferedStream = require("io-stream-buffered");
var validate =         require("validate");
var string =           require("string-validation");
var object =           require("object-validation");
var boolean =          require("boolean-validation");
var array =            require("array-validation");
var type =             require("type");
var version =          require("version");

var PKX_SYSTEM =              "pkx";
var PKX_FILE_EXTENSION =      "." + PKX_SYSTEM;
var PKX_DESCRIPTOR_FILENAME = "package.json";
var PROTOCOL_PKX =            PKX_SYSTEM;
var DEPENDENCY_PKX =          PKX_SYSTEM;
var DEPENDENCY_CONFIG =       "configuration";
var DEPENDENCY_REQUIRER =     "requirer";
var INDENT_OFFSET =           4;

var ERROR_UNSUPPORTED_OPERATION =     "Unsupported Operation";
var RROR_TARGET_MISMATCH =            "Target Mismatch";
var ERROR_DEPENDENCY =                "Dependency Error";
var ERROR_NO_ENTRY_POINT =            "No Entry Point";
var ERROR_INVALID_PKX_SELECTOR =      "Invalid Selector";
var ERROR_INVALID_PKX_DESCRIPTOR =    "Invalid Package Descriptor";
var ERROR_INVALID_REQUEST_PROCESSOR = "Invalid Request Processor";
var ERROR_INVALID_PKX_VOLUME =        "Invalid PKX Volume";

var init = false;
var processors = {};
var processing = {};
var volumes =    [];
var requested =  {};

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////////////////
function findVolume(selector) {
    var id = selector.package + "/" + (selector.resource? selector.resource : "");
    for (var v in volumes) {
        if (version.compare(id, v, selector.upgradable) || id == v) {
            return volumes[v];
        }
    }
}

function mountVolume(uri, options) {
    return new Promise(function(resolve, reject) {
        if (typeof uri == "string") {
            uri = ioURI.parse(uri);
        }
    
        // validate arguments
        if (!(uri instanceof ioURI)) {
            throw new TypeError("Mandatory parameter 'uri' should be of type 'URI'.");
        }
    
        var gzipStream;
        var tarStream;
        var tarVolume;
        var pkxJSONStream;
        var pkxVolume;
        uri.open()
            .then(openGzip)
            .then(getTarFromGzip)
            .then(openTar)
            .catch(reject)
            .then(openPackageJSON)
            .then(readPackageJSON)
            .then(parsePackageJSON)
            .finally(closeStreamsAndResolve)
            ;
    
        function openGzip(stream) {
            gzipStream = stream;
    
            if (options.headers && gzipStream.headers) {
                gzipStream.headers = options.headers;
            }
    
            return gzip.open(gzipStream);
        }
        function getTarFromGzip(gzipReader) {
            return new Promise(function(resolve, reject) {
                for(var t=0;t<gzipReader.metadata.tags.length;t++) {
                    if (gzipReader.metadata.tags[t].type == gzip.GZIP_TAG_TYPE_OBJECT) {
                        stream = gzipReader.getGZipObjectStream(gzipReader.metadata.tags[t].value);
                        return resolve(stream);
                    }
                }
                throw new Error(ERROR_INVALID_PKX_VOLUME, "The gzip stream does not seem to contain a file.");
            });
        }
        function openTar(stream) {    
            tarStream = stream;
            return tar.open(stream, options.strip? { "strip" : options.strip } : null);
        }
        function openPackageJSON(volume) {
            tarVolume = volume;
            return volume.open("/" + PKX_DESCRIPTOR_FILENAME);
        }
        function readPackageJSON(stream) {
            pkxJSONStream = stream;
            return stream.readAsJSON();
        }
        function parsePackageJSON(pkxJSON) {
            return new Promise(function(resolve, reject) {
                // validate pkx
                var pkx = new PKX(pkxJSON);
                    
                pkxVolume = new PKXVolume(tarVolume, pkx);

                io.volumes.register(pkxVolume);

                /*if (!volumes[own.pkx.id]) {
                    volumes[own.pkx.id] = own;
                }
                else {
                    volumes[own.pkx.id + " <" + volumes.length + ">"] = own;
                    //if (console) {
                    //    console.warn("A PKXVolume with id '" + own.pkx.id + "' was already mounted and still present in cache.");
                    //}
                }*/

                resolve();
            });
        }
        function closeStreams() {
            if (pkxJSONStream) {
                pkxJSONStream.close().catch(console.warn);
            }
            if (gzipStream) {
                gzipStream.close().catch(console.warn);
            }

            resolve(pkxVolume);
        }
    });
}

function loader(request, handler) {
    var selector;
    var pkxVolume;
    var packageId;
    var mounting = {};
    var waiters = [];

    var resolve;
    var reject;

    function handleRequest(callback, fail) {
        resolve = callback;
        fail = reject;

        // check target
        try {
            validate(selector.target, object)
                .when(object.compare, host);
        }
        catch(e) {
            if (selector.optional) {
                // gracefully stop
                resolve(null, true);
                return;
            }
            else {
                throw new Error(ERROR_TARGET_MISMATCH, "Target does not match, and the request is not optional.", selector.target);
            }
        }

        // change relative paths for embedded resources
        if (selector.package.substr(0, 2) == "./" && handler && handler.context) {
            selector.package = "pkx:///" + handler.context.id.substr(0, handler.context.id.indexOf("/")) + (selector.package.length > 2 ? "/" + selector.package.substr(2) : "");
        }

        // send the request off to the processors (ex. url replacement, variable substitution, ...)
        // if the processors are already processing a similar selector, it will not be processed again,
        // but rather wait until it is doen.
        for (var p in processors) {
            var promise = processors[p](selector);
            if (!promise) {
                continue;
            }
            if (processing[selector.package]) {
                // subscribe to existing processor
                processing[selector.package].addEventListener("ready", function(sender, a) { 
                    // the selector uri could be updated, so propagate the update
                    selector.uri = processing[selector.package].selector.uri;
                    fetchModule(a); }
                );
                processing[selector.package].addEventListener("error", function(sender, e) { 
                    error(e);
                } );
                return;
            }

            // create new event emitter
            processing[selector.package] = new event.Emitter(promise);
            processing[selector.package].selector = selector;
            
            function createReadyCallback(id) {
                return function(a) { processing[id].fire("ready", a); delete processing[id]; fetchModule(a); }
            }
            function createErrorCallback(id) {
                return function(e) { processing[id].fire("error", e); error(e); }
            }

            promise.then(createReadyCallback(selector.package), createErrorCallback(selector.package));
            return;
        }

        // no processor was found to prepare the request
        fetchModule();
    }
    function fetchModule(options) {
        // check define cache for existing module
        if (typeof define != "undefined" && define.using && !selector.raw) {
            var cached = define.cache.get(selector.id, selector.upgradable);
            if (cached) {
                completeRequest(cached);
                return;
            }
        }

        // if scheme is pkx, then get the mounted volume for the cache
        if (selector.uri.scheme == "pkx") {
            for (var v in volumes) {
                if (selector.uri.path && volumes[v].pkx.id == selector.uri.path.substr(1)) {
                    pkxVolume = volumes[v];

                    getDependencies();
                    return;
                }
            }
        }

        // get existing volume for package
        packageId = selector.package + "/" + (selector.resource? selector.resource : "");
        pkxVolume = findVolume(selector);

        // if the volume was already mounted, proceed
        if (pkxVolume) {
            getDependencies();
            return;
        }

        // if the volume is already mounting, wait for the mounting to finish
        if (mounting[packageId]) {
            mounting[packageId].waitFor(function(volume) {
                pkxVolume = volume;
                getDependencies();
            });
            return;
        }

        // add waiting mechanism for other requests for this volume (to avoid mulitple mounting of the same volume)
        mounting[packageId] = {
            "callbacks" : [],
            "waitFor" : function(callback) {
                this.callbacks.push(callback);
            },
            "ready" : function(volume) {
                while(this.callbacks.length > 0) {
                    this.callbacks[0](volume);
                    this.callbacks.splice(0, 1);
                }
                delete mounting[packageId];
            }
        };

        // mount the pkx volume
        mountVolume(selector.uri, options).then(function (volume) {
            pkxVolume = volume;

            // execute all callbacks witing for this volume
            mounting[packageId].ready(pkxVolume);

            getDependencies();
        }, error);

    }
    function getDependencies() {
        var requests = [];
        var pkxDeps = pkxVolume.pkx.pkx.dependencies;
        if (selector.raw && selector.ignoreDependencies) {
            getResourceStreamFromVolume();
            return;
        }
        else {
            for (var d in pkxDeps) {
                switch (type.getType(pkxDeps[d])) {
                    case type.TYPE_OBJECT:
                        if (pkxDeps[d].system &&
                            pkxDeps[d].system != PKX_SYSTEM) {
                            requests[d] = pkxDeps[d];
                        }
                    // fallthrough intended
                    case type.TYPE_STRING:
                        requests[d] = new PKXSelector(pkxDeps[d]);
                        if (selector.raw) {
                            requests[d].wrap = selector.wrap;
                            requests[d].raw = true;
                        }
                        break;
                    default:
                        // unknown system
                        requests[d] = pkxDeps[d];
                }

                // modify relative uri for embedded packages
                var embedded;
                if (requests[d].package.substr(0, 2) == "./") {
                    requests[d].package = "pkx:///" + pkxVolume.pkx.id + (requests[d].package.length > 2 ? "/" + requests[d].package.substr(2) : "");
                    embedded = true;
                }

                if (requests[d].wrap) {
                    requests[d].ignoreCache = true;
                }

                // skip circular, embedded dependencies
                var selUri = selector.uri.toString();
                selUri = selUri.substr(0, selUri.indexOf("/",7)) || selector.uri.toString();
                if (embedded && selector.uri.scheme == "pkx" && selUri == "pkx:///" + pkxVolume.pkx.id) {
                    requests[d] = null;
                }
            }
        }

        // filter out removed dependencies
        var filtered = [];
        for (var r in requests) {
            if (requests[r]) {
                filtered.push(requests[r]);
            }
        }
        requests = filtered;

        // require all dependencies
        using.apply(this, requests).then(getResourceStreamFromVolume, function(loader) {
            var halt;
            var mods = [];
            for (var r in loader.requests) {
                if (loader.requests[r].err.length > 0 && !loader.requests[r].request.optional) {
                    halt = true;
                }
                else {
                    mods.push(loader.requests[r].module);
                }
            }
            if (!halt) {                                  
                getResourceFrompkxVolume.apply(this, mods);
                return;
            }
            error(new Error(ERROR_DEPENDENCY, "", loader));
        }, true);
    }
    function getResourceStreamFromVolume() {
        // find out which resource to load
        var resource = null;
        if (selector.resource) {
            resource = selector.resource;
        }
        else {
            // get first matching main (by target)
            if (type.isString(pkxVolume.pkx.pkx.main)) {
                resource = pkxVolume.pkx.pkx.main;
            }
            else if (pkxVolume.pkx.pkx.main) {
                for (var m in pkxVolume.pkx.pkx.main) {
                    if(pkxVolume.pkx.pkx.main[m].target) {
                        try {
                            validate(pkxVolume.pkx.pkx.main[m].target, object)
                                .when(object.compare, host);
                            resource = pkxVolume.pkx.pkx.main[m].resource;
                        }
                        catch(e) {
                            // ignore for now
                        }
                    }
                }
            }
        }

        if (!resource) {
            error(new Error(ERROR_NO_ENTRY_POINT, "Package '" + pkxVolume.pkx.id + "' does not have an entry point defined for the current target."));
            return;
        }

        // prepend slash to resource name if not present
        if (resource.substr(0,1) != "/") {
            resource = "/" + resource;
        }

        var dependencies = [];
        for (var a=0;a<arguments.length;a++) {
            // dependency module
            dependencies[a] = arguments[a];
        }

        if (!requested[pkxVolume.pkx.id + resource] && !selector.raw) {
            requested[pkxVolume.pkx.id + resource] = true;
        }
        else if (!selector.raw) {
            define.cache.waitFor(pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/"), complete);
            return;
        }

        pkxVolume.open(resource).then(function readDataFromResourceStream(stream) {
            if (selector.raw && !selector.wrap) {
                completeRequest(stream, dependencies);
                return;
            }
            stream.readAsString().then(function processCode(data) {
                var name = stream.getName();
                var ext = name.substr(stream.getName().lastIndexOf(".") + 1);
                var raw = ext != "js" && ext != "json" && (ext != "css" || (ext == "css" && !host.isRuntimeBrowserFamily() && host.runtime != host.RUNTIME_NWJS));

                // wrap code for define
                if (ext == "js") {
                    data = loader.wrap(data, pkxVolume.pkx, selector.resource, pkxVolume.pkx.pkx.dependencies, selector.configuration, host.runtime != host.RUNTIME_NODEJS, pkxVolume.pkx.id + resource, selector.raw && selector.wrap);
                }

                if (selector.raw || raw) {
                    completeRequest(new ioBufferedStream(data.toUint8Array()), dependencies);
                    return;
                }

                // add dependencies
                define.parameters = {};
                define.parameters.system = PKX_SYSTEM;
                define.parameters.id = pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/");
                define.parameters.pkx = pkxVolume.pkx;
                define.parameters.dependencies = [ DEPENDENCY_PKX, "module", DEPENDENCY_CONFIG ];
                define.parameters.dependencies[0] = pkxVolume.pkx;
                for(var d in dependencies) {
                    define.parameters.dependencies.push(dependencies[d]);
                }

                if (ext == "js") {
                    // load code
                    if (host.isRuntimeNodeFamily() && !host.isRuntimeBrowserFamily()) {
                        try {
                            require("vm").runInThisContext(data, {filename: (selector.uri.scheme == "file"? process.cwd() : "") + resource, lineOffset: -1});
                        }
                        catch (e) {
                            error(e);
                            return;
                        }
                        completeRequest(null, null, true);
                    }
                    else if (host.isRuntimeBrowserFamily()) {
                        var script = document.createElement("script");
                        script.language = "javascript";
                        script.type = "text/javascript";
                        script.text = data;
                        try {
                            document.body.appendChild(script);
                        }
                        catch (e) {
                            error(e);
                            return;
                        }
                        completeRequest(null, null, true);
                    }
                    else {
                        error(new Error("Loading code in runtime '" + host.runtime + "' is not supported."));
                    }
                }
                if (ext == "json") {
                    var json;
                    try {
                        json = JSON.parse(data);
                    }
                    catch (e) {
                        error(e);
                        return;
                    }
                    completeRequest(json, dependencies);
                }
                if (ext == "css") {
                    if (typeof document !== "undefined") {
                        data += "\r\n/*# sourceURL=http://" + (pkxVolume.pkx.id + resource) + "*/";
                        var style = document.createElement("style");
                        style.rel = "stylesheet";
                        style.type = "text/css";
                        if (style.styleSheet) {
                            style.styleSheet.cssText = data;
                        }
                        else {
                            style.appendChild(document.createTextNode(data));
                        }
                        try {
                            document.head.appendChild(style);
                        }
                        catch (e) {
                            error(e);
                            return;
                        }
                    }
                    completeRequest(data, dependencies);
                }
            }, error);
        }, error);
    }
    function error(e) {
        loaderObj.err.push(e);

        if (!loaderObj.module && pkxVolume) {
            loaderObj.module = new define.Module();
            loaderObj.module.id = (pkxVolume.pkx? pkxVolume.pkx.id : (request.package? request.package : request)) + (request.resource || (pkxVolume.pkx && pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/")? request.resource : "/");
            if (pkxVolume.pkx) {
                loaderObj.module.parameters = {
                    "id":     pkxVolume.pkx.id + (request.resource ? request.resource : "/"),
                    "system": PKX_SYSTEM,
                    "pkx":    pkxVolume.pkx
                }
            }
        }

        resolve();
        return;
    }
    function completeRequest(fact, dependencies, checkWait) {
        // check for delayed loading
        if (checkWait && define.parameters.wait) {
            waiters =  define.parameters.wait;

            for (var w in waiters) {
                waiters[w].then(completeRequest, error);
            }
            return;
        }
        else {
            // wait for all waiters to complete
            var allDone = true;
            for (var w in waiters) {
                if (!waiters[w].done) {
                    allDone = false;
                    break;
                }
            }
            if (!allDone) {
                return;
            }
        }

        if (!fact) {
            fact = define.cache.get(pkxVolume.pkx.id + (request.resource? request.resource : "/"));
        }
        else if (!(fact instanceof define.Module)) {
            var mod = new define.Module();
            mod.id = pkxVolume.pkx.id + (selector.resource || pkxVolume.pkx.id.substr(pkxVolume.pkx.id.length - 1) == "/"? selector.resource : "/");
            mod.factory = fact;
            mod.dependencies = dependencies || [];
            mod.parameters = {
                "id" : pkxVolume.pkx.id + (request.resource? request.resource : "/"),
                "system" : PKX_SYSTEM,
                "pkx" : pkxVolume.pkx
            };
            fact = mod;
        }

        resolve(fact);
        return;
    }

    // validate request
    try {
        selector = new PKXSelector(request);
    }
    catch(e) {
        if (e instanceof Error && e.name == ERROR_INVALID_PKX_SELECTOR) {
            throw new RangeError(e.message);
        }
        else {
            throw e;
        }
    }

    // return a loader
    var loaderObj = new define.Loader(request, handleRequest);
    return loaderObj;
}
loader.factory = function(module, factory, request, requirer) {
    // decorate dependencies
    var dependencies = module.dependencies.slice(0);
    var args = [];

    // add package object to front if there was no string left in dependencies to process
    var addPkx = true;
    for (var d in dependencies) {
        if (!isNaN(d)) {
            if (Object.prototype.toString.call(dependencies[d]) !== "[object String]") {
                if (dependencies[d] === module.parameters.pkx) {
                    addPkx = false;
                    break;
                }
            }
        }
    }

    if (addPkx) {
        args.push(module.parameters.pkx);
    }

    // Replace the pkx descriptor or pkx placeholder with an instance of PKX, unless type, string and validate
    // are not loaded yet. This means that when they are first instantiated by this module, they will not have
    // the functions available to a PKX instance. This is not a disaster since those base libraries do not use
    // them. Any instances created after the initial load will have them available, except if they're singleton.
    for (var d in dependencies) {
        if ((dependencies[d] === module.parameters.pkx ||
            !isNaN(d) && dependencies[d] == DEPENDENCY_PKX) &&
            type && string && object && array && boolean && validate) {
            dependencies[d] = new PKX(dependencies[d]);
        }
        if (!isNaN(d) && dependencies[d] == DEPENDENCY_CONFIG) {
            dependencies[d] = request && request.configuration? request.configuration : (module.parameters.configuration? module.parameters.configuration : {});
        }
        if (!isNaN(d) && dependencies[d] == DEPENDENCY_REQUIRER) {
            dependencies[d] = requirer;
        }
    }

    // add other dependencies (only numbered index)
    for (var a=0;a<dependencies.length;a++) {
        args.push(dependencies[a]);
    }

    // execute module factory
    return factory.apply(factory, args);
};
loader.wrap = function(code, pkx, resourceId, dependencies, configuration, addSourceMap, sourceMapName, isEmbedded) {
    if (!resourceId) {
        resourceId = "/";
    }

    var depStr = "";

    if (isEmbedded) {
    }

    // stringify the package definition (pretty print)
    var descriptor = JSON.stringify(pkx, null, Array(INDENT_OFFSET + 1).join(" "));

    // generate module definition code
    var moduleCode = "(function(module, using, require) {\n";
    if (isEmbedded) {
        // add configuration
        configuration = configuration? "\ndefine.parameters.configuration = " + JSON.stringify(configuration, null, Array(INDENT_OFFSET + 1).join(" ")) + ";" : "";

        // add package dependency
        depStr += "\ndefine.parameters.dependencies = [ \"" + DEPENDENCY_PKX + "\", \"module\", \"" + DEPENDENCY_CONFIG + "\", \"" + DEPENDENCY_REQUIRER + "\" ];";
        depStr += "\ndefine.parameters.dependencies[0] = define.parameters.pkx;";

        if (dependencies) {
            for (var t = 0; t < dependencies.length; t++) {
                var dep = dependencies[t];
                if (dep) {
                    var resName = dep + "/";
                    if (Object.prototype.toString.call(dep) === "[object Object]") {
                        resName = dep.package + (dep.resource ? dep.resource : "/");
                    }
                    depStr += "\ndefine.parameters.dependencies.push(define.cache.get(\"" + resName +  "\"));";
                }
            }
        }

        moduleCode += "define.parameters = {};\n" + (isEmbedded? "define.parameters.wrapped = true;\n" : "") + "define.parameters.system = \"" + PKX_SYSTEM + "\";\ndefine.parameters.id = \"" + pkx.id + resourceId + "\";\ndefine.parameters.pkx = " + descriptor + ";" + depStr + configuration + "\n";
    }
    moduleCode += "define.prepare();\n\n";
    moduleCode += "using = define.getUsing(define.parameters.id);\n";
    moduleCode += "require = define.getRequire(define.parameters.id, require);\n";
    moduleCode += code + "\n\nif(module.exports) {\n    define(function factory() { return module.exports; });\n}\n})({},typeof using != \"undefined\"? using : null, typeof require != \"undefined\"? require : null);" + (addSourceMap? "\n//# sourceURL=http://" + (sourceMapName? sourceMapName : (pkx.id + resourceId)) : "");

    // add code indent to make it pretty
    var moduleLines = moduleCode.split(/\r*\n/);
    var indentWhiteSpace = Array(INDENT_OFFSET + 1).join(" ");

    // add header to make it pretty
    if (isEmbedded) {
        moduleCode = "/////////////////////////////////////////////////////////////////////////////////////\n";
        moduleCode += "//\n";
        moduleCode += "// module '" + pkx.id + resourceId + "'\n";
        moduleCode += "//\n";
        moduleCode += "/////////////////////////////////////////////////////////////////////////////////////\n";
        moduleCode += moduleLines[0] + "\n";
        for (var l=1;l<moduleLines.length - (addSourceMap? 2 : 1);l++) {
            moduleCode += indentWhiteSpace + moduleLines[l] + "\n";
        }
        if (addSourceMap) {
            moduleCode += moduleLines[moduleLines.length - 2] + "\n";
        }
        moduleCode += moduleLines[moduleLines.length - 1] + "\n";
    }

    return moduleCode;
};
loader.addRequestProcessor = function(name, fn) {
    if (processors[name]) {
        throw new Error(ERROR_INVALID_REQUEST_PROCESSOR, "A processor with name '" + name + "' is already registered.");
    }
    processors[name] = fn;
};

/////////////////////////////////////////////////////////////////////////////////////////////
//
// PKXSelector Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function PKXSelector(selector, validateId) {
    var own = this;
    var errName = ERROR_INVALID_PKX_SELECTOR;
    var addDefaultResource = false;
    var valid = true; // indicates invalid naming pattern
    var majorVersion = null;
    var minorVersion = null;
    var patchVersion = null;
    var repository = null;

    switch(type.getType(selector)) {
        case type.TYPE_STRING:
            selector = { 
                "package" : selector
            };
            addDefaultResource = true;
            break;
        case type.TYPE_OBJECT:
            break;
        default:
            throw new Error(errName, "Mandatory parameter 'selector' should be of type 'String' or 'Object'.");
    }

    validate(selector.package, string, errName, "package")
        .when(string.isURL);

    // verify naming pattern
    var idxSlash = selector.package.lastIndexOf("/", selector.package.length - 2);
    // when a slash is detected trim off pkx extension if present
    var parts = (idxSlash >= 0? selector.package.substring(idxSlash + 1, selector.package.lastIndexOf(PKX_FILE_EXTENSION) == selector.package.length - PKX_FILE_EXTENSION.length? selector.package.length - PKX_FILE_EXTENSION.length: null) : selector.package).split(".");
    if (parts.length > 0 && parts[parts.length - 1].substr(parts[parts.length - 1].length - 1) == "/") {
        parts[parts.length - 1] = parts[parts.length - 1].substr(0,parts[parts.length - 1].length - 1);
    }
    // if package does not contain any slashes, assume it is an id
    if (selector.package.indexOf("/") == -1) {
        if (!isNaN(parts[parts.length - 1]) && !isNaN(parts[parts.length - 3])) {
            patchVersion = isNaN(parts[parts.length - 1]);
        }
        minorVersion = parts[parts.length - (isNaN(parts[parts.length - 3]) ? 1 : 2)];
        if (isNaN(minorVersion)) {
            minorVersion = null;
            valid = false;
        }
        majorVersion = parts[parts.length - (isNaN(parts[parts.length - 3]) ? 2 : 3)];
        if (isNaN(majorVersion)) {
            majorVersion = null;
            valid = false;
        }
    }
    if (validateId && !valid) {
        throw new Error(errName, "Mandatory property 'package' does not match the " + PKX_SYSTEM + " naming pattern.");
    }
    validate(selector.resource, string, errName)
        .when(string.isPath);
    validate(selector.target, object, errName);
    validate(selector.raw, boolean, errName);
    validate(selector.wrap, boolean, errName);
    validate(selector.system, string, errName);
    validate(selector.optional, boolean, errName);

    // normalise path (add leading slash)
    if (selector.resource && selector.resource.length > 0 && selector.resource.substr(0,1) != "/") {
        selector.resource = "/" + selector.resource;
    }

    var uri;
    var vers = {};
    Object.defineProperty(vers, "major", {
        get: function() {
            return majorVersion;
        }
    });
    Object.defineProperty(vers, "minor", {
        get: function() {
            return minorVersion;
        }
    });
    Object.defineProperty(vers, "patch", {
        get: function() {
            return patchVersion;
        }
    });
    Object.defineProperty(this, "version", {
        get: function() {
            return vers;
        }
    });
    Object.defineProperty(this, "id", {
        get: function() {
            return own.package + (addDefaultResource? "/" : (own.resource || ""));
        }
    });
    Object.defineProperty(this, "name", {
        get: function() {
            // get full name (strip version numbers from id string)
            var name = "";
            var nameParts = own.package.split(".");
            for (var i = 0; i < nameParts.length; i++) {
                if (isNaN(nameParts[i])) {
                    name += (name != "" ? "." : "") + nameParts[i];
                }
            }
            return name;
        }
    });
    Object.defineProperty(this, "repository", {
        get: function() {
            return repository;
        }
    });
    Object.defineProperty(this, "uri", {
        get: function() {
            return uri;
        },
        set: function(u) {
            if (typeof u === "string") {
                uri = ioURI.parse(replaceVariables(u));
            }
            else if ((u instanceof ioURI)) {
                uri = u;
            }
            else {
                throw new Error("The URI should be of type 'String' or 'URI'.");
            }
        }
    });
    Object.defineProperty(this, "isArchive", {
        get: function() {
            return own.package.lastIndexOf("/") != own.package.length - 1;
        }
    });
    this.package = selector.package;
    this.resource = selector.resource;
    this.target = selector.target;
    this.raw = selector.raw || false;
    this.wrap = selector.wrap || false;
    this.system = selector.system;
    this.optional = selector.optional || false;
    this.upgradable = selector.upgradable || (using? using.UPGRADABLE_NONE : null);
    this.ignoreDependencies = selector.ignoreDependencies || false;
    this.configuration = selector.configuration || null;

    this.parseURI = function(u, namespaceSeperator) {
        return ioURI.parse(replaceVariables(u, namespaceSeperator));
    };

    function replaceVariables(u, namespaceSeperator) {
        // add .pkx extension
        var uriPKXName = (namespaceSeperator? own.package.replace(/\./g,namespaceSeperator) : own.package) + (own.package.lastIndexOf(PKX_FILE_EXTENSION) != own.package.length - PKX_FILE_EXTENSION.length && own.isArchive ? PKX_FILE_EXTENSION : "");
        // replace variables
        return u.replace(/\$NAME_NO_NS/g, (namespaceSeperator? own.name.replace(/\./g,namespaceSeperator) : own.name).substr(own.repository && own.repository.namespace && own.repository.namespace.length > 0? own.repository.namespace.length + 1 : 0))
            .replace(/\$NAME/g, (namespaceSeperator? own.name.replace(/\./g,namespaceSeperator) : own.name))
            .replace(/\$PATCH/g, patchVersion)
            .replace(/\$MINOR/g, minorVersion)
            .replace(/\$MAJOR/g, majorVersion)
            .replace(/\$PACKAGE/g, uriPKXName)
            .replace(/\$ID/g, (namespaceSeperator? own.package.replace(/\./g,namespaceSeperator) : own.package));
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// PKX Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function PKX(descriptor){
    var own = this;
    var errName = ERROR_INVALID_PKX_DESCRIPTOR;

    switch(type.getType(descriptor)) {
        case type.TYPE_STRING:
            try {
                descriptor = JSON.parse(descriptor);
            }
            catch(e) {
                throw new Error(errName, "Could not parse the JSON string.", e);
            }
            break;
        case type.TYPE_OBJECT:
            break;
        default:
            throw new Error(errName, "Mandatory parameter 'descriptor' should be of type 'String' or 'Object'.");
    }

    var name = [ string.LOWERCASE_LETTER, string.DIGIT, string.DASH ];
    validate(descriptor.name, string, errName)
        .allow(name, string.DOT)
        .when(descriptor.name != "")
        .notNull();
    validate(descriptor.version, string, errName)
        .when(string.isSemVer)
        .notNull();
    validate(descriptor.title, string, errName);
    validate(descriptor.description, string, errName);
    validate(descriptor.pkx, object, errName)
        .notNull();

    // merge all descriptor properties
    own = type.merge(descriptor, this);

    Object.defineProperty(own, "id", {
        get : function() {
            return own.name + "." + own.version;
        }
    });

    // return modified object
    return own;
}

/////////////////////////////////////////////////////////////////////////////////////////////
//
// PKXVolume Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function PKXVolume(volume, pkx) {
    // validate arguments
    if (!(volume instanceof tar.TarVolume)) {
        throw new TypeError("Mandatory parameter 'volume' should be of type 'TarVolume'.");
    }
    if (!(pkx instanceof PKX)) {
        throw new TypeError("Mandatory parameter 'pkx' should be of type 'PKX'.");
    }

    this.id =          pkx.id + PKX_FILE_EXTENSION;

    this.protocol =    PROTOCOL_PKX;
    this.description = "Package " + pkx.id;
    this.state =       ioVolume.STATE_INITIALIZING;
    this.size =        volume.size;
    this.type =        ioVolume.TYPE_REMOVABLE;
    this.scope =       ioVolume.SCOPE_LOCAL;
    this.class =       ioVolume.CLASS_TEMPORARY;
    this.readOnly =    true;
    this.pkx =         pkx;

    // bind to source volume functions
    this.open =              Function.prototype.bind.call(volume.open,              volume);
    this.delete =            Function.prototype.bind.call(volume.delete,            volume);
    this.query =             Function.prototype.bind.call(volume.query,             volume);
    this.getBytesUsed =      Function.prototype.bind.call(volume.getBytesUsed,      volume);
    this.getBytesAvailable = Function.prototype.bind.call(volume.getBytesAvailable, volume);
    this.close =             Function.prototype.bind.call(volume.close,             volume);
}
PKXVolume.prototype = ioVolume;

/////////////////////////////////////////////////////////////////////////////////////////////
//
// URI Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
var mod = {};
mod.parse = function(uri) {
    if(string.isURL(uri)) {
        if (uri.length >= 5 && uri.substr(0,5) == PROTOCOL_PKX + "://") {
            return new ioURI(uri, self);
        }
    }
};
mod.open = function(uri, opt_access) {
    if (uri && type.isString(uri)) {
        uri = mod.parse(uri);
    }
    else if (uri && (typeof uri.scheme== "undefined" || typeof uri.path == "undefined")) {
        uri = null;
    }
    var idxSlash = uri.path.indexOf("/", 1);
    var volId = uri.path? uri.path.substring(1, idxSlash > -1 ? idxSlash : undefined) : null;
    var resId = uri.path? uri.path.substr(volId.length + 1) : null;
    if (!volumes[volId]) {
        throw new Error(ERROR_INVALID_PKX_VOLUME, "PKXVolume '" + volId + "' is not mounted.");
    }
    return volumes[volId].open(resId, opt_access);
};
mod.exists = function(uri) {
    return new Promise(function(resolve, reject) {
        reject(new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support querying files from pkx volumes."));
    });
};
mod.toString = function(uri) {
    return uri.toString();
};
mod.delete = function(uri) {
    return new Promise(function(resolve, reject) {
        reject(new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support deleting files from pkx volumes."));
    });
};
mod.getTemp = function() {
    throw new Error(ERROR_UNSUPPORTED_OPERATION, "The pkx module does not support creating temp files.");
};

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Register PKX Protocol Handler
//
/////////////////////////////////////////////////////////////////////////////////////////////
ioURI.protocols.register(mod, PROTOCOL_PKX);

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Register PKX Extension
//
/////////////////////////////////////////////////////////////////////////////////////////////
if (typeof define === "function" && define.cache && define.using) {
    try {
        define.Loader.register(PKX_SYSTEM, loader);
    }
    catch(e) {
        if (!(e instanceof RangeError)) {
            throw e;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = mod;